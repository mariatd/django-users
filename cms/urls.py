from django.urls import path
from . import views

urlpatterns = [
	path('', views.home, name='home'),
	path('acceso/', views.inicio_sesion),
	path('logout/', views.cerrar_sesion),
	path('<str:recurso>/', views.gestionar_recurso),
]
