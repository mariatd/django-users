from django.db import models

# Create your models here.

class Contenido(models.Model):
    clave = models.CharField(max_length=64, unique=True)
    valor = models.TextField()

    def __str__(self):
        return f"{self.id}: {self.clave}"

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    comentario = models.TextField(blank=False)
    fecha_publicacion = models.DateTimeField('Fecha de publicación')

    def __str__(self):
        return f"{self.id}: {self.titulo} --- {self.contenido.clave}"
